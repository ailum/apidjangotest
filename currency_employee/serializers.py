from rest_framework import serializers
from .models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee # this is the model that is being serialized
        fields = ('id', 'name',
                  'contractTypeName','roleId','roleDescription','hourlySalary',
                  'monthlySalary','annualSalary')
