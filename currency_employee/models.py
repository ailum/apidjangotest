from django.db import models

class Employee(models.Model):
        
        HOURLY='Hourly Salary Contract'
        MONTHLY='Monthly Salary Contract'
        
        CATEGORIES = (
                (HOURLY, 'Hourly Salary Contract'),
                (MONTHLY, 'Monthly Salary Contract'),
        )
        
        id=models.IntegerField(primary_key=True)
        name=models.CharField(max_length=150)
        contractTypeName=models.CharField(max_length=200, choices=CATEGORIES)
        roleId=models.IntegerField()
        roleDescription=models.CharField(max_length=200)
        hourlySalary=models.DecimalField(decimal_places=2, max_digits=6)
        monthlySalary=models.DecimalField(decimal_places=2, max_digits=6)
        annualSalary=models.DecimalField(decimal_places=2, max_digits=6)
