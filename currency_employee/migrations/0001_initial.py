# Generated by Django 3.2.3 on 2021-05-16 23:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=150)),
                ('contractTypeName', models.CharField(choices=[('Hourly Salary Contract', 'Hourly Salary Contract'), ('Monthly Salary Contract', 'Monthly Salary Contract')], max_length=200)),
                ('roleId', models.IntegerField()),
                ('roleDescription', models.CharField(max_length=200)),
                ('hourlySalary', models.DecimalField(decimal_places=2, max_digits=6)),
                ('monthlySalary', models.DecimalField(decimal_places=2, max_digits=6)),
                ('annualSalary', models.DecimalField(decimal_places=2, max_digits=6)),
            ],
        ),
    ]
