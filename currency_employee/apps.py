from django.apps import AppConfig


class CurrencyEmployeeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'currency_employee'
